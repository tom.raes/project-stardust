# Project Stardust

The ultimate SWGOH theory-crafting tool.

Brainchild of CLASH from https://discord.gg/zKbeMd8.

### How to import new spreadsheet data
To upload a new snapshot of data into the tool, first go to the google sheet (https://docs.google.com/spreadsheets/d/13WRvu824lxZ9HWiEnqAGheJpjtOV8KN6Fd3mY9bSSms/edit?usp=sharing)
and select File... Download... CSV. 

Then, input that CSV to this online tool: https://www.csvjson.com/csv2json

See this image for the correct configuration settings: ![](img/csv-to-json.png).

Next, paste the result into the databank.js file, being sure to wrap it in the global variable declaration like:
`window.databank = {JSON INSIDE HERE};`

Finally, commit and push your change to the master branch, which should automatically trigger the deployment job.

### Project TODO List
- consider tooltips over portraits. what should be inside?
- add categories to separate portraits
- try icons-only for effects at mobile breakpoint
- fix overlapping of death star at wider breakpoints (needs a max-size defined)
- make sure all characters have properly defined image relationships (may be a jawa thats broken)
- provide a call-to-action in right-side of screen when no checkboxes are checked
- fix issue where on mobile the focus is persistent and makes checkboxes look bad